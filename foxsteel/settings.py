"""
Django settings for foxsteel project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'rl3(talu89)ndza*8ngh-4ntfpghl_r6djlg3!m+0(@!i%d78!'

# SECURITY WARNING: don't run with debug turned on in production!
SETTINGS = "prod"

DEBUG = True
TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ["*"]


# Application definition

INSTALLED_APPS = (
    'grappelli',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'blog',
    'catalogo',
    'tastypie',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'foxsteel.urls'

WSGI_APPLICATION = 'foxsteel.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres://zoiyyjqc:qMr5Y-IXECLTqHQULJpvdB1bai_AncNO@babar.elephantsql.com:5432/zoiyyjqc',

        #'NAME': 'foxsteel',
    }
}



# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'es-mx'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

if SETTINGS == "prod":
    import dj_database_url
    DATABASES["default"] = dj_database_url.config()
    SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")


import os
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = "static"

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoryFinder',
    'djangular.finders.NamespacedAngularAppDirectoriesFinder'
)

TEMPLATE_DIRS = (
    "templates"
)

#Grappelli admin title
GRAPPELLI_ADMIN_TITLE = "ADMINISTRADOR DE FOXSTEEL"

# Tastypie configuration
TASTYPIE_DEFAULT_FORMATS = ["json"]
API_LIMIT_PER_PAGE = 10
