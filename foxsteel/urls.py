from django.conf.urls import patterns, include, url
import grappelli
from catalogo.api import ProductoResource
from blog.api import BlogResource
from tastypie.api import Api
from django.views.generic import TemplateView

api = Api(api_name = "v1")
api.register(ProductoResource())
api.register(BlogResource())

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^grappelli/', include('grappelli.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', TemplateView.as_view(template_name = "base.html")),
    #url(r'^nosotros/$', 'website.views.nosotros', name = "nosotros"),
    #url(r'^contacto/$', 'website.views.contacto', name = "contacto"),
    #url(r'^blog/$', 'blog.views.posts', name = "posts"),
    #url(r'^blog/(?P<slug>\D+)/$', 'blog.views.post', name = "post"),
    #url(r'^productos/$', 'website.views.productos', name = 'productos'),
    #url(r'^productos/(?P<slug>\S+)/$', 'website.views.producto', name = 'producto'),

    # Api resources
    url(r'^api/', include(api.urls)),

)
