'use_strict';

/**
* galleryApp Module
*
* This is an angular app to show a product catalog gallery
*/
var galleryApp = angular.module('galleryApp', ['ngRoute', 'ngSanitize', 'galleryAppControllers']).
	config(['$httpProvider', '$routeProvider',
		function($httpProvider, $routeProvider) {
			$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';			

			$routeProvider.
			when('/', {
				templateUrl: '../static/catalogo/app/partials/index.html',
				controller: 'IndexCtrl'				
			}).
			when('/nosotros', {
				templateUrl: '/static/catalogo/app/partials/nosotros.html'
			}).
			when('/productos', {
				templateUrl: '/static/catalogo/app/partials/productos.html',
				controller: 'ProductListCtrl'
			}).			
			when('/productos/:productoSlug', {
				templateUrl: '/static/catalogo/app/partials/producto.html',
				controller: 'ProductCtrl'
			}).
			when('/productos/categorias/:categoria', {
				templateUrl: '/static/catalogo/app/partials/categoria.html',
				controller: 'CategoriaFilterCtrl'
			}).
			when('/blog', {
				templateUrl: '/static/catalogo/app/partials/blog.html',
				controller: 'BlogCtrl'
			}).
			when('/blog/:postSlug', {
				templateUrl: '/static/catalogo/app/partials/post.html',
				controller: 'PostCtrl'
			}).
			when('/contacto', {
				templateUrl: '../static/catalogo/app/partials/contacto.html',
				//controller: 'ContactoCtrl'
			}).
			otherwise({
				redirectTo: '/'
			});

	}]);

