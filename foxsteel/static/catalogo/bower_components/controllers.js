'use strict';

/**
* galleryAppControllers Module
*
*/
var galleryAppControllers = angular.module('galleryAppControllers', []);

galleryAppControllers.controller('IndexCtrl', ['$http', '$scope', 
	function($http, $scope){
		$http.get('/api/v1/productos/').success(function(data) {

			// Obtener productos con campo producto_estrella
			$scope.productos = data.objects;
			$scope.limitNumber = 8;
			$scope.orderProp = "id";

			// Obtener los últimos 8 productos
			$scope.nuevos = data.objects;
			$scope.limitNumber = 5;
			$scope.orderProp = "-id";
		});
}]);

galleryAppControllers.controller('ProductListCtrl', ['$http', '$scope', 
	function($http, $scope){
		$http.get('/api/v1/productos/').success(function(data) {

			// Listado de productos principal de la galería
			$scope.productos = data.objects;
		});

		$http.get('/api/v1/productos/?producto_estrella=true').success(function(data) {
			$scope.limitNumber = 8;
			$scope.orderProp = "-id"
			$scope.recomendados = data.objects;
		});
}]);

galleryAppControllers.controller('CategoriaFilterCtrl', ['$http', '$scope', '$routeParams', 
	function($http, $scope, $routeParams){
		$http.get('/api/v1/productos/?categoria=' + $routeParams.categoria).success(function(data){
			$scope.productos = data.objects;
		});

		$http.get('/api/v1/productos/?producto_estrella=true').success(function(data) {
			$scope.limitNumber = 8;
			$scope.orderProp = "-id"
			$scope.recomendados = data.objects;
		});
}]);

galleryAppControllers.controller('ProductCtrl', ['$http', '$scope', '$routeParams', '$sce',
	function($http, $scope, $routeParams, $sce){
		$http.get('/api/v1/productos/?slug=' + $routeParams.productoSlug).success(function(data) {
			$scope.producto = data.objects[0];			
		});

		$http.get('/api/v1/productos/?producto_estrella=true').success(function(data) {
			$scope.limitNumber = 8;
			$scope.orderProp = "-id"
			$scope.recomendados = data.objects;
		});
}]);

galleryAppControllers.controller('BlogCtrl', ['$http', '$scope', 
	function($http, $scope){
		$http.get('/api/v1/posts/').success(function(data) {

			// Listado de posts
			$scope.posts = data.objects;
		});

		$http.get('/api/v1/posts/').success(function(data) {
			$scope.limitNumber = 5;
			$scope.orderProp = "-id"
			$scope.posts = data.objects;
		});
}]);

galleryAppControllers.controller('PostCtrl', ['$http', '$scope', '$routeParams', 
	function($http, $scope, $routeParams){
		
		// Información de cada post
		$http.get('/api/v1/posts/?slug=' + $routeParams.postSlug).success(function(data) {		
			$scope.post = data.objects[0];
		});

		$http.get('/api/v1/posts/').success(function(data) {
			$scope.limitNumber = 5;
			$scope.orderProp = "-id"
			$scope.posts = data.objects;
		});
}]);

