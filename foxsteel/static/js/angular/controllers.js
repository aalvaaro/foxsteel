var catalogControllers = angular.module('catalogControllers', []);

catalogControllers.controller('ProductListController', function($scope, $http) {
	$http.get('/api/v1/productos/').success(function(data) {
		$scope.productos = data.objects;
	});
});