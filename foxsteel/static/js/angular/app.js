var catalogApp = angular.module('catalogApp', [
		'ngRoute',
		'catalogControllers'
	]);

catalogApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: '/static/html/angular/productos/listado-productos.html',
        controller: 'ProductListController'
      }).      
      otherwise({
        redirectTo: '/'
      });
  }]);







