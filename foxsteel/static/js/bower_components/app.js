var galleryApp = angular.module('galleryApp', [
		'ngRoute',
		'galleryAppControllers'
	]).

	config(function ($httpProvider, $routeProvider) {
		$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
		$routeProvider.
			when('/', {
				templateUrl: 'template/index.html',
				controller: 'ProductListController'
			}).
			when('/detail', {
				templateUrl: 'template/detail.html',
				controller: 'ProductDetailController'
			}).
			otherwise({
				redirectTo: '/'
			});
	});





/*galleryApp.controller('galleryAppController', function($scope, $http) {
		
	$http.get('/api/v1/producto/').success(function(data) {
		$scope.productos = data.objects;
	});

});*/


