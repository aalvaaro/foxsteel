from tastypie.resources import ModelResource, ALL
from tastypie import fields
from .models import Post

class BlogResource(ModelResource):
    class Meta:
        queryset = Post.objects.all()
        resource_name = "posts"
        list_allowed_methods = ["get"]
        filtering = {
            "slug": ALL,
            "id": ALL,
        }

