from django.shortcuts import render_to_response
from .models import Post
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

def posts(request):
    """ User paginator.Paginator to show a few posts only"""
    posts_list = Post.objects.all()
    paginator = Paginator(posts_list, 2) # Show 2 posts per page

    page = request.GET.get("page")
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page
        posts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results
        posts = paginator.page(paginator.num_pages)

    # Product cathegories
    context = {"posts": posts}
    return render_to_response("blog.html", context)

def post(requets, slug):
    post = Post.objects.get(slug = slug)
    posts = Post.objects.values("titulo")[:10]
    context = {"post": post, "posts": posts}
    return render_to_response("blog_detail.html", context)
