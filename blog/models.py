from django.db import models
from autoslug import AutoSlugField

class Post(models.Model):
    titulo = models.CharField(max_length = 100)
    fecha = models.DateField(auto_now_add = True)
    imagen = models.CharField(max_length = 300, help_text = "URL de la imagen (facebook, imageshack, etc)")
    contenido = models.TextField()
    slug = AutoSlugField(populate_from = "titulo", always_update = True,
                         unique_with = "id")

    def __unicode__(self):
        return self.titulo
