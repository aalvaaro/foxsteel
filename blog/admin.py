from django.contrib import admin
from .models import Post

class PostAdmin(admin.ModelAdmin):
    list_filter = ["titulo", "fecha"]
    search_fields = ("titulo", "fecha", "contenido",)

    class Media:
        js = ("js/tiny_mce/tiny_mce.js",
              "js/basic_config.js",)

admin.site.register(Post, PostAdmin)

