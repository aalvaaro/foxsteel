from django.contrib import admin
from .models import Producto

class ProductoAdmin(admin.ModelAdmin):
	list_filter = ["nombre", "subcategoria"]
	search_fields = ("nombre", "subcategoria",)

	class Media:
		js = ("js/tiny_mce/tiny_mce.js",
			  "js/basic_config.js",)


admin.site.register(Producto, ProductoAdmin)




