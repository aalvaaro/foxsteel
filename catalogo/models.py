# -*- coding: utf-8 -*-

from django.db import models
from autoslug import AutoSlugField

class Producto(models.Model):
    nombre = models.CharField(max_length = 100)
    imagen = models.URLField()
    introduccion = models.CharField(max_length = 200)
    caracteristicas = models.TextField()
    dimensiones = models.TextField()
    consumos = models.TextField()
    producto_estrella = models.BooleanField(help_text = "Marcar si es producto estrella",
                                            default = False)
    slug = AutoSlugField(populate_from = "nombre", always_update = True,
                         unique_with = "id")
    categoria = models.CharField(max_length = 100)
    subcategoria = models.CharField(max_length = 100)

    def __unicode__(self):
        return self.nombre


