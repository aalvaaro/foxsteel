# -*- coding: utf-8 -*-

from tastypie.resources import ModelResource, ALL
from tastypie import fields
from .models import Producto

# Use this way for nested values
"""class CategoriaResource(ModelResource):
    class Meta:
        queryset = Categoria.objects.all()
        resource_name = "categoria"

class SubcategoriaResource(ModelResource):
    categoria = fields.ForeignKey(CategoriaResource, "categoria", full_list = True,
                                  full = True, full_detail = True)

    class Meta:
        queryset = Subcategoria.objects.all()
        resource_name = "subcategoria"
"""

class ProductoResource(ModelResource):
    """subcategoria = fields.ForeignKey(SubcategoriaResource, "subcategoria", full_list = True,
                                     full = True, full_detail = True)
    """
    class Meta:
        queryset = Producto.objects.all()
        resource_name = "productos"
        list_allowed_methods = ["get"]
        filtering = {
            "slug": ALL,
            "categoria": ALL,
            "id": ALL,
            "producto_estrella": ALL
        }



