from django.shortcuts import render, render_to_response
from catalogo.models import Producto

def index(request):
    estrellas = Producto.objects.filter(producto_estrella = True)
    nuevos = Producto.objects.all().order_by("-id")[:10]
    context = {"estrellas": estrellas, "nuevos": nuevos}
    return render_to_response("index.html", context)

def nosotros(request):
    return render_to_response("nosotros.html")

def productos(request):
    productos = Producto.objects.all()
    estrellas = Producto.objects.filter(producto_estrella = True)[0:5]
    context = {"productos": productos, "estrellas": estrellas}
    return render_to_response("productos.html", context)

def producto(request, slug):
    producto = Producto.objects.get(slug = slug)
    estrellas = Producto.objects.filter(producto_estrella = True)[:8]
    nuevos = Producto.objects.all().order_by("-id")[:5]
    context = {"producto": producto, "estrellas": estrellas,
               "nuevos": nuevos}
    return render_to_response("producto.html", context)

def contacto(request):
    return render_to_response("contacto.html")
